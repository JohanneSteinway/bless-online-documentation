.. _doc_x_about_guilds:

About Guilds
=============

I hope we all share the desire to see our guild to grow and prosper. In such a case there are some things 
that are good to know when we start playing the game. When the servers open and someone will register our 
guild it will be small and limited in number of players to host (we can have only 10 members at level 1 
though we have already recruited more than 40 as of 14th of May, e.i. 2 weeks before launch) as well 
as in capabilities and bonuses it can provide. In order to utilize maximum potential of the guild we’ll 
need to put an effort into leveling it.


Good thing to know is that you do not need to be super high level or wear a legendary gear in order to 
contribute to the guild growth. Literally all we do in game can help guild to grow and prosper. For guild 
to grow we need to accumulate as many guild influence points (the actual name might be different for Steam 
version, but I’ll update it as soon as we’ll get hands on the game) as possible. Though we are limited by 
2000 points per member each day. This cap is not hard to reach once you know how to do that. And if all 
members will make their contribution the guild will grow and get stronger pretty fast.

What generates these Guild Influence Points? There are three other types of in game points that directly 
contribute toward these points:

1.	Adventure points

2.	Dungeon points

3.	Honor points

Don’t know yet if the ratio will remain the same for the Steam version but in earlier versions 100 of any 
points in these three categories were earning 1 guild influence point. Good to know that while farming these 
points you not only level up your guild, but it’s beneficial to you personally. It is officially confirmed that 
you are able to exchange these points for lumena and use them for purchase of valuable resources for crafting 
and upgrading gear.

So what is each of these 3 categories of points?


Adventure Points
+++++++++++++++++

.. image:: img/x_about_guilds_adventure_points.jpg
    :width: 80%
    :align: center

You generally get these for most of PvE activities. The best ways being:

**First Method**

Hunting quests. Extremely effective and fast when done in a group or raid. You generally gather group and come 
to any location with several sorts of mobs from bestiary (section in quest log page). Worth noting, that if you 
do the hunting quests the first time you’ll have three tiers of a quest for each species of mobs with increasing 
rewards. Once you have completed all three tiers you can reset the last tier once per day and do it again. So yes 
you can contribute daily and while doing that you gain adventure points for yourself as well as resources for gear 
upgrades. So win + win situation.

**Second Method**

Resource gathering. While you run around the world and see ore nods, or gatherable plants on your way, do not 
hesitate to grab them – each node gathered will generate adventure points plus pleasant bonus resources that can 
be sold or used for crafting. Now here start some tricks that you might not know at the beginning and by using these 
things we can really make a difference. Once in a while instead of ordinary resource node you’ll stumble into so 
called “turban”. They had a fixed spawn points earlier and were contested pretty harsh, but since JP version mechanics 
have been changed – now they spawn anywhere where resource nods are – just randomly instead of the node. It looks just 
like a colorful cap with gems laying on the ground. 

Once you activate that cap aka turban you are teleported inside a 
special mini dungeon where you’ll find two NPCs standing at the fork of tunnels leading to the three different caves. 
One NPC can teleport you out of this dungeon to several locations of your choice. But before using him you should talk 
to his brother standing next to him as he’ll activate an event: all three caves that are nearby will spawn a huge amount 
of resources in each of it. And you’ll have 3 minutes to gather anything you see. Each nod gathered generates adventure 
points and resources. And some rumors say (it has never been confirmed officially) that these nodes have higher chances 
to generate rare resources. Sounds good? We did not even come to good part yet :-) before entering the turban you need 
to make some preparations – better in advance, while you are still in a capital. 

.. image:: img/x_about_guilds_adventure_points2.jpg
    :width: 80%
    :align: center

Once you’ll accumulate some adventure points, visit adventure point vendor and check what he has to offer. You’ll find 
a very useful consumable which decreases the gathering time by 100 % for one minute. That means that once you consume it you’ll 
be gathering nodes immediately. If you’ll bring 3 of those consumables to the turban, you’ll be able to gather more than 2 caves 
of resources in those 3 minute that you are given – believe me – once you’ll try it you’ll never want to do turban the other 
way ;-) Now you’ll probably ask me was it that “good part” I promised… well, it kind of was, but the best part I kept for the end 
;-) The same NPC we bought speed cookies from sells one more consumable. 

It doubles generation of all adventure points for period 
of one week. It is bit expensive at the start, but once you’ll have the needed amount and there will still be some 
adventure points left for speed cookies, buy it without hesitation and consume. It will pay off without any doubts. If 
you’ll use combo of these two types of consumables and farm like 10 turbans and some other activities for your pleasure – 
you’ll already cap in your daily guild points. And though at first sight speed cookies and point generation boost seem to 
be expensive, once used you’ll start generating crazy amounts of adventure points and not only cover your needs but also 
have enough to support guild new-bros (and sisters even more :-)  )

Dungeon Points
+++++++++++++++

.. image:: img/x_about_guilds_dungeon_points.jpg
    :width: 80%
    :align: center

This part is pretty much straight forward. You complete dungeon runs, you get dungeon points. Fraction of these points generate 
guild points as well. These are not that fast and easy to gather as adventure points, but you’ll probably be doing dungeons at 
some point anyway so you’ll get these points too. Some things are good to know in advance though: the higher level dungeons give 
more dungeon points, so don’t get overexcited farming low dungeon – as always better things are just around the corner ;-) and 
keep in mind that the best you can get from the mobs is when you are +/- 3levels of their level. Another important thing which 
you should do before going to the dungeon, try to tame and get a pet with the skill that gives bonus dungeon points – you won’t 
ever get too much of these, you know, don’t you :-)

Honor Points
+++++++++++++

.. image:: img/x_about_guilds_honor_points.jpg
    :width: 80%
    :align: center

Well, it seems all should be pretty clear here too – you grow up as big as you can (well, don’t grow too big 
and too green or you might have hard time recapping yourself back :-)), grab the biggest bad ass weapon you can 
carry and get smashing the skulls of enemies. The more you smash, the more honor points you get. Pretty simple, 
ha? But who would I be if I would not have some tricks for you here as well :-) First of all you can skip the part 
of becoming Hulk all together as in order to generate good amount of honor points you don’t need to grow big (aka 
get level cap). 

Once you enter PvP battleground, like Castra Siege aka 100x100, you’ll be surprised to find out 
that you became max level for the duration of battle and all your base stats are appropriate for the max level. 
And considering that you’ll have up to 100 skulls to smash there it is arguably the best place to get these 
shiny honor points (by the way you get them for kills, for heals, for damage absorbed… pretty much anything you do… 
well almost anything – you won’t get much for sightseeing there… but what a views there are when 100 angry men 
(sometimes cat girls) clash into your comrades :-). But well, we were talking about bit other things this time… 

Even if you are not fan of PvP and all into PvE (as I am myself) I strongly recommend you to go for these Castra 
sieges once you ding level 10 (or whichever will be the lowest for entry on Steam version). Believe me, you won’t 
spend your time in game better and more useful than there. And one more thing – once inside, make sure you join up 
into raid with other guildies or even other allies as being part of the raid (the bigger, the better – size matters 
in this case) and moving together will ensure more heals for you (or you’ll earn more points for healing others if 
you are healer), you’ll be safer, kill more enemies and overall your results will grow so high that even that big 
green guy will get ever greener out of the jealousy :-). 

.. image:: img/x_about_guilds_honor_points2.jpg
    :width: 80%
    :align: center

And bare in mind, that while fighting you won’t only generate honor points. Once a week you’ll get a love letter 
from a secret fan who was watching your battles all week long and he’ll send you a pretty valuable prize for your efforts.


If you are blood thirsty bastard and can’t have enough suffering of your enemies you can go to Bazel Valley after 
Castra siege (or before) and do PvP dailies there with good chance to smash more skulls in a process.

And yes of course you can earn some honor points by fighting in an open world and killing who ever moves (never forget 
to cut each grass and bush, that waves in the wind – who knows what hides behind it :-)). But these kills wont save you if 
you can’t fill you flood thirst. Never the less each kill will count toward the extreme growth of guild eXtreme toward 
its extreme destiny in extreme world of Bless Online :)