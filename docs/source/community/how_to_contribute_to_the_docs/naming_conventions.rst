.. _doc_naming_conventions:

Naming Conventions
==================

Section Hierarchy
+++++++++++++++++

Every word in a section starts with a Capital letter. And the symbols need to be at least as long as the text.

.. tabs::

    .. code-tab:: rst Title

        Document Title (Every document need to have one)
        =================================================
    
    .. code-tab:: rst Big Section


        Big Section
        +++++++++++


    .. code-tab:: rst Medium Section

        Medium Section
        --------------

    .. code-tab:: rst Tiny Section

        Tiny Section (doesn't appear on tree)
        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Reference Naming
+++++++++++++++++

Every document starts with a reference that look like this: ``.. _doc_filename:``. Except the index files.
Other references use the syntax: ``.. _ref_referencename:``

File Naming
+++++++++++

Every file name consists of lower case letters and instead of spaces words separated by ``_`` (underscores). 
Every document has the ``.rst`` extension.
Image file names starts with the name of the file it's going to be used in followed by a describing title for
the image, the extension can be either ``.jpg`` (if it has no transparency), ``.png`` (if it has transparency) 
or ``.gif`` (if it's animated).

Folder Structure
+++++++++++++++++

Folders use the same naming conventions as files. The folders in the main directory represent the Categories, 
the folders inside them represent Subcategories. The folders always share the name of its categorys name.