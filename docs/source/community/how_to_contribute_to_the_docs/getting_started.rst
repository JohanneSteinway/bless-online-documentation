.. _doc_getting_started:

Getting Started
===============

Prerequisites
++++++++++++++

You will need a GitLab account if you don't already have one you can register 
`Here <https://gitlab.com/users/sign_in>`_.
After you have a an account go to the `Bless Online Docs gitlab 
page <https://gitlab.com/xKeiro/bless-online-documentation>`_ and
click on the **Fork** button.

.. figure:: img/getting_started_fork.jpg
    :width: 100%
    :align: center

After that just follow the instructions shown. When the fork completed go to 
**Settings-> Repository-> Pull from a remote repository**. Click on **Expand**, 
tick the **Mirror repository** box and write ``https://gitlab.com/xKeiro/bless-online-documentation.git``
to the **Git repository URL** field. You don't have to change anything else, go ahead and click on the
**Save Changes** button. This ensures that your version of the documentation stays up to date.

.. figure:: img/getting_started_auto_update.jpg
    :width: 100%
    :align: center

Congratulations now you are finally ready to contribute to the docs!

Your first contribution
++++++++++++++++++++++++

Find a topic you are interested in and would like to add something to it, and then let's make sure 
that you are on the latest version of the document you wish to edit.
You can do this by looking at the bottom left of the documentation. Change the **v:stable** to **latest**.
(Or if you'd lke to, you can change the branches on Gitlab instead, from **stable** to **master**.)

.. figure:: img/getting_started_switch_version.jpg
    :width: 100%
    :align: center

Next, click on the **Edit on GitLab** button at the top right corner of the page. 

.. image:: img/getting_started_edit_on_gitlab.jpg

Once you are on GitLab you can see the content of the selected article, a few things may appear differently
since we use reStructuredText (and not Markdown) for formatting for the docs, but more on this later. For now
just click on either **Edit** or **Web IDE** to start editing.

.. warning::
    Make sure that you are on the "master" branch before hitting Edit as mentioned before.

.. figure:: img/getting_started_how_to_edit.jpg
    :width: 100%
    :align: center

Here you'll be able to edit/add what you wanted, once you done write a short summary about what you changed to
the **Commit message** field and press the **Commit Changes** button.

.. figure:: img/getting_started_how_to_commit.jpg
    :width: 70%
    :align: center

After this click on the the **Submit merge request** button. (Optionally: tick the "Allow edits from maintainers")

.. image:: img/getting_started_submit_merge_request.jpg

Congratulations you just made your first contribution to the project! All you have to do now is wait until
somebody approves your edits.